﻿using UnityEngine;
using System.Collections;

public class AnswerItem : Entity
{
    public RoomType type;
    public Vector3 originPos;
    public Transform originalParent;

    void Start()
    {
        originPos = this.transform.position;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        Entity trigger = col.GetComponent<Entity>();
        if(trigger is FireBehaviour)
        {
            print("In the fire");
            this.transform.parent = col.transform;
        }        
    }
}
