﻿using UnityEngine;
using System.Collections;

public enum RoomType
{
    ANIMAL,
    PEOPLE,
    FOOD,
    RPG_ITEMS,
    ELEMENTS
}
