﻿using UnityEngine;
using System.Collections;
using System.Xml;

public static class RoomXmlLoader{

    /// <summary>
    /// Load the XML document of the rooms, load a random node into a Room object and return it
    /// </summary>
    /// <returns></returns>
    public static Room GetRandomRoom()
    {
        TextAsset textAsset = (TextAsset)Resources.Load("XML/Rooms");

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(textAsset.text);
        XmlNamespaceManager xmlnsMngr = new XmlNamespaceManager(xmlDoc.NameTable);
        XmlNodeList groupElements = xmlDoc.DocumentElement.SelectNodes("/RoomInfo/Room", xmlnsMngr);

        System.Random rand = new System.Random();
        int randomNum = rand.Next(0, groupElements.Count);

        int cardIdentifier = 0;

        foreach (XmlNode room in groupElements)
        {
            string riddleText = "";
            string answerText = "";
            RoomType type;
            string answerItemName = "";

            try
            {
                type = StringToRoomType(room.FirstChild.InnerText);
                riddleText = room.FirstChild.NextSibling.InnerText;
                answerText = room.FirstChild.NextSibling.NextSibling.InnerText;
                answerItemName = room.FirstChild.NextSibling.NextSibling.NextSibling.InnerText;

                if (cardIdentifier == randomNum)
                {
                    return new Room(type, riddleText, answerText, answerItemName);
                }
            }
            catch(System.Exception ex)
            {
                Debug.LogError("Failed to load room; " + ex.Message);
            }

            cardIdentifier++;
        }
        return null;

    }

    /// <summary>
    /// Given a descriptive string, return the appropriate room type
    /// </summary>
    /// <param name="typeText">The description of the room type</param>
    /// <returns></returns>
    private static RoomType StringToRoomType(string typeText)
    {
        switch (typeText)
        {
            case("ANIMAL"):
                return RoomType.ANIMAL;

            case("PEOPLE"):
                return RoomType.PEOPLE;

            case("FOOD"):
                return RoomType.FOOD;

            case("ELEMENTS"):
                return RoomType.ELEMENTS;

            case("RPG_ITEMS"):
                return RoomType.RPG_ITEMS;

            default:
                throw new System.ArgumentException("Unknown type string " + typeText);
        }
    }
}
