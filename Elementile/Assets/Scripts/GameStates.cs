﻿using UnityEngine;
using System.Collections;

public enum GameStates
{
    ENTERROOM,
    RIDDLE,
    PLAY,
    RESULT,
	WIN,
    LOSE,
    LEAVEROOM    
}
