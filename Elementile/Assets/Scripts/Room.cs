﻿using UnityEngine;
using System.Collections;

public class Room
{
    private RoomType type;
    private string riddleText;
    private string riddleAnswer;
    private string riddleAnswerItemName;

    /// <summary>
    /// Constructor taking all required information for a new room
    /// </summary>
    /// <param name="type">The type of room</param>
    /// <param name="riddleText">The text of the riddle in that room</param>
    /// <param name="riddleAnswer">The answer to the riddle</param>
    /// <param name="riddleAnswerItemName">The name of the object in "Resources/Room Objects/" to load as the gameobject</param>
    public Room(RoomType type, string riddleText, string riddleAnswer, string riddleAnswerItemName)
    {
        this.type = type;
        this.riddleText = riddleText;
        this.riddleAnswer = riddleAnswer;
        this.riddleAnswerItemName = riddleAnswerItemName;
        Debug.Log(riddleAnswerItemName);
    }

    /// <summary>
    /// Returns the RoomType of this room
    /// </summary>
    /// <returns></returns>
    public RoomType GetRoomType()
    {
        return type;
    }

    /// <summary>
    /// Returns the riddle text for this room
    /// </summary>
    /// <returns></returns>
    public string GetRiddleText()
    {
        return riddleText;
    }


    /// <summary>
    /// Returns the riddle answers for this room
    /// </summary>
    /// <returns></returns>
    public string GetRiddleAnswer()
    {
        return riddleAnswer;
    }

    /// <summary>
    /// Returns the gameobject that when plavced in the fire in this room should be accepted as the correct answer
    /// </summary>
    /// <returns></returns>
    public GameObject GetRiddleAnswerItem()
    {        
        GameObject ansObj = Resources.Load("Room Objects/" + riddleAnswerItemName) as GameObject;

        if (ansObj == null)
        {
            throw new System.NullReferenceException("Riddle answer object requested does not exist: " + riddleAnswerItemName);
        }

        return ansObj;
    }
}