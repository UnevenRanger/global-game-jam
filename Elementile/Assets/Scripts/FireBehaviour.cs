﻿using UnityEngine;
using System.Collections;

public class FireBehaviour : Entity
{

    public Room thisRoom;
    public ParticleSystem normalFire;
    public ParticleSystem losingFire;
    public ParticleSystem winningFire;
    public Sprite portalOn, portalOff;
    public Animator roomDoor;
    public PlayerMovementScript thePlayer;
    public AudioSource correctSound;
    public AudioSource incorrectSound;
    public AudioSource burnItemSound;

    private GameObject thePortal;
    private SpriteRenderer portalRenderer;
    // Use this for initialization
    void Start()
    {
        normalFire.Play();
        thePortal = GameObject.Find("Portal");
        portalRenderer = thePortal.GetComponent<SpriteRenderer>();
        portalRenderer.sprite = portalOff;
    }

    // Update is called once per frame
    void Update()
    {
        AnswerItem placedItem = GetComponentInChildren<AnswerItem>();
        if (placedItem != null)
        {
            CheckItemInFire(placedItem.gameObject);
        }
    }

    public bool CheckItemInFire(GameObject item)
    {
        burnItemSound.Play();
        bool success = false;
        string[] itemName = item.name.Split('(');        

        if (item.GetComponent<AnswerItem>() != null)
        {            
            GameObject.Destroy(item);
            normalFire.Stop();
            if (itemName[0] == thisRoom.GetRiddleAnswerItem().name)
            {
                correctSound.Play();
                //do fire animation and open door
                winningFire.Play();
                //roomDoor.SetInteger("AnimState",1); //This will need testing
                portalRenderer.sprite = portalOn;
                thePortal.GetComponent<PortalScript>().guessedCorrect = true;

            }
            else
            {
                incorrectSound.Play();
                losingFire.Play();
                thePlayer.playerHealth--;
                //do fire animation and lose health                                
                //thePlayer.LoseHealth();
            }
        }
        return success;
    }

}
