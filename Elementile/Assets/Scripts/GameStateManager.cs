﻿using UnityEngine;
using System.Collections;

public class GameStateManager : MonoBehaviour
{
    GameStates state;
    ObjectSpawner objSpawner;
    DialogueTextManager diaManager;

    public bool isPlayerDead;



	void Start ()
    {
        objSpawner = GetComponent<ObjectSpawner>();
        diaManager = GetComponent<DialogueTextManager>();
        state = GameStates.ENTERROOM;
	}
	
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.F))
        {
            print(state);
        }

	    switch(state)
        {
            case GameStates.ENTERROOM:
                //Choose a riddle and load in the answer objects atop the pedestals
                if(objSpawner.objectsSpawned == true)
                {
                    state = GameStates.RIDDLE;
                }
                break;
            case GameStates.RIDDLE:
                if(diaManager.textPanelClosed == true)
                {
                    state = GameStates.PLAY;
                }
                break;
            case GameStates.PLAY:
                //player is free to move around and place objects in the fire.
                break;
            case GameStates.RESULT:
                //when an object is placed in the fire, check to see if it is the correct one.
                break;
            case GameStates.WIN:
                //the correct object was placed in the fire. The demon congratulates the player and the door to the next room opens.
                break;
            case GameStates.LOSE:
                //the wrong object was placed in the fire. The demon taunts the player.
                break;
            case GameStates.LEAVEROOM:
                //load a new room.
                break;
        }
	}

    public GameStates GetState()
    {
        return this.state;
    }
}
