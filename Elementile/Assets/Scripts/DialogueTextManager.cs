﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialogueTextManager : MonoBehaviour
{
    public GameObject UiArea;
    private Text theText;
    
    private string rawText = null;
    private string[] textLines;

    private int currentLine = 0;
    private int endAtLine = 0;

    public bool textPanelClosed;

    void Start()
    {
        textPanelClosed = false;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.H))
        {
            print("TGing");
        }
        if (Input.GetKeyDown(KeyCode.Space) && rawText != null)
        {
            print("Detected");
            DisplayNextLine();
        }        
    }

    public void SetDialogue(string text)
    {
        print("SetDialogue called");
        if (theText == null)
        {
            theText = UiArea.GetComponent<Text>();
        }
        currentLine = 0;
        endAtLine = 0;
        rawText = text.ToUpper();
        ParseRawText();
        DisplayNextLine();
    }

    private void ParseRawText()
    {
        if (rawText != null)
        {
            textLines = (rawText.Split('\n'));
        }
        else
        {
            throw new System.NullReferenceException("No string set");
        }

        if (endAtLine == 0)
        {
            endAtLine = textLines.Length - 1;
        }
    }

    public void DisplayNextLine()
    {
        if (rawText != null && textLines != null)
        {
            if (currentLine > endAtLine)
            {
                rawText = null;
                textLines = null;
                currentLine = 0;
                UiArea.transform.parent.transform.parent.gameObject.SetActive(false);
                textPanelClosed = true;
            }
            else
            {
                theText.text = textLines[currentLine];
                currentLine += 1;
            }
        }
    }
}
