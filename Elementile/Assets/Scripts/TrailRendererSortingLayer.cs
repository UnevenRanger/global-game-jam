﻿using UnityEngine;
using System.Collections;

public class TrailRendererSortingLayer : MonoBehaviour {

    void Start()
    {
        GetComponent<TrailRenderer>().GetComponent<Renderer>().sortingLayerName = "Pedestal Item";
        GetComponent<TrailRenderer>().GetComponent<Renderer>().sortingOrder = 2;
    }
}
