﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ObjectSpawner : MonoBehaviour
{
    private List<AnswerItem> objectsArray;
    private Room loadedRoom;
    
    private DialogueTextManager dialogText;
    public FireBehaviour roomFire;
    
    public bool objectsSpawned;

	void Start ()
    {
        dialogText = GetComponent<DialogueTextManager>();
        objectsSpawned = false;
        objectsArray = new List<AnswerItem>(GetSpawnPedestals().Length);
        loadedRoom = RoomXmlLoader.GetRandomRoom();
        roomFire.thisRoom = loadedRoom;
        FillObjectsArray();
        SpawnObjects();
        dialogText.SetDialogue(loadedRoom.GetRiddleText());
	}
	
    /// <summary>
    /// Spawn all the interactive objects in this scene
    /// </summary>
    public void SpawnObjects()
    {
        foreach(Pedastal pedastal in GetSpawnPedestals())
        {
            if (objectsArray.Count != 0)
            {
                SpawnObjAtPedastal(pedastal);
            }
        }
        objectsSpawned = true;
    }

    /// <summary>
    /// Spawn an object from the objectsArray at the given pedestal and make the spawned object a child of that pedestal
    /// </summary>
    /// <param name="currPedestal">Pedastal to spawn the object at</param>
    private void SpawnObjAtPedastal(Pedastal currPedestal)
    {
        int selection = Random.Range(0,objectsArray.Count);
        Instantiate(objectsArray[selection],
            new Vector3(currPedestal.transform.position.x, currPedestal.transform.position.y, currPedestal.transform.position.z),
            transform.rotation);
        //might be good if the spawned obj was child of the pedestal, then the selection logic can be in the pedestal script
        objectsArray.RemoveAt(selection);
    }

    /// <summary>
    /// Get all the pedestal objects from the scene
    /// </summary>
    /// <returns></returns>
    private Pedastal[] GetSpawnPedestals()
    {
        Pedastal[] pedastals = GameObject.FindObjectsOfType<Pedastal>();
        return pedastals;
    }

    /// <summary>
    /// Load the objects of the same type as the room into the objectsArray
    /// </summary>
    private void FillObjectsArray()
    {
        GameObject[] loadedGameObjs = Resources.LoadAll<GameObject>("Room Objects");
        bool haveAnswer = false;

        foreach (GameObject currObject in loadedGameObjs)
        {
            if (currObject.GetComponents<AnswerItem>() != null)
            {
                if (currObject.GetComponent<AnswerItem>().type == loadedRoom.GetRoomType())
                {
                    AnswerItem currAnsItem = currObject.GetComponent<AnswerItem>();
                    objectsArray.Add(currAnsItem);
                    if (currObject == loadedRoom.GetRiddleAnswerItem())
                    {
                        haveAnswer = true;
                    }
                }
            }
        }

        if (!haveAnswer)
        {
            loadedGameObjs = null;
            FillObjectsArray();
        }
    }
}