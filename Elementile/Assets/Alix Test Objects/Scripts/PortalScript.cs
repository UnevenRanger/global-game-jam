﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PortalScript : MonoBehaviour
{
    public bool guessedCorrect;
    public AudioSource teleportSound;

    void Start()
    {
        guessedCorrect = false;
    }
    void OnTriggerEnter2D()
    {
        if(guessedCorrect == true)
        {
            teleportSound.Play();
            StartCoroutine("LoadScene");
        }
    }

    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(0.2f);
        print("ReloadingScene");
        SceneManager.LoadScene(1);
    }
}
