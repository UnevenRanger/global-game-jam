﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelLoader : MonoBehaviour
{
    public Animator optionsAnimate;
    private GameObject fireHolder, optionsExitButton;

    void Start()
    {
        fireHolder = GameObject.Find("Fire_normal");
        optionsExitButton = GameObject.Find("OptionsExitButton");
        optionsExitButton.SetActive(false);
    }

    public void LoadLevel(int chosenLevel)
    {
        if (chosenLevel == 1)
        {
            SceneManager.LoadScene(1);
        }
        if(chosenLevel == 2)
        {
            optionsAnimate.SetInteger("OptionsState", 1);
            Invoke("StayOpen", 0.9f);

        }
        if(chosenLevel == 3)
        {
            Application.Quit();
        }
        if(chosenLevel == 4)
        {
            optionsAnimate.SetInteger("OptionsState", 3);
            optionsExitButton.SetActive(false);
            Invoke("StayClosed", 0.9f);
        }
    } 
    
    void StayOpen()
    {
        optionsAnimate.SetInteger("OptionsState", 2);
        optionsExitButton.SetActive(true);
        fireHolder.SetActive(false);
    }

    void StayClosed()
    {
        optionsAnimate.SetInteger("OptionsState", 0);
        fireHolder.SetActive(true);
    }
}
