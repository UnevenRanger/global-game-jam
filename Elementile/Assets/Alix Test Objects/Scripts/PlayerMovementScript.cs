﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerMovementScript : MonoBehaviour
{
    public float speedModifier = 0.6f;
    //public float pickupDist = 0.7f;
    public Transform fire;
    public Sprite playerFront, playerBack, playerLeft, playerRight;
    public GameObject dropItemRenderer;
    public GameObject staffEffect;
    public GameStateManager stateManager;
    public AudioSource stepSound, pickUpItem;
    public int playerHealth = 3;
    public Image heart1, heart2, heart3;

    private List<Transform> ansItems;    
    private AnswerItem heldItem;

    protected Transform m_transform; //Why is this protected??
    private SpriteRenderer playerRenderer; //Made this private. Is that to your standard Sam??

    Ray2D playerRay;
    RaycastHit2D playerHit;

	void Start()
    {        
        m_transform = transform;
        ansItems = new List<Transform>();
        playerRenderer = gameObject.GetComponent<SpriteRenderer>();
        playerRenderer.sprite = playerBack;

        foreach (AnswerItem pedastal in FindObjectsOfType<AnswerItem>())
        {
            ansItems.Add(pedastal.transform);
        }
    }

    void Update()
    {
        if (stateManager.GetState() == GameStates.PLAY || stateManager.GetState() == GameStates.WIN)
        {
            DoMovementChecks();

            if (stateManager.GetState() == GameStates.PLAY)
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    if (heldItem == null)
                    {
                        CastRayForward();
                    }
                    else
                    {
                        DropHeldItem();
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                GameObject staffEffectObj = (GameObject)Instantiate(staffEffect, this.transform.position, this.transform.rotation);
                staffEffect.GetComponent<ParticleSystem>().Play();
            }
        }

        if (playerHealth == 2)
        {
            heart3.enabled = false;
        }
        else if (playerHealth == 1)
        {
            heart3.enabled = false;
            heart2.enabled = false;
        }else if (playerHealth == 0)
        {
            heart3.enabled = false;
            heart2.enabled = false;
            heart1.enabled = false;
            stateManager.isPlayerDead = true;
        }
    }

    private void DoMovementChecks()
    {
        bool didMove = false;

        if (Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D))
        {
            didMove = true;
            playerRenderer.sprite = playerBack;
            m_transform.rotation = Quaternion.Euler(0, 0, 0);
            m_transform.position += new Vector3(0, 0.1f * speedModifier, 0);

            if (heldItem != null)
            {
                heldItem.transform.localPosition = new Vector3(0.23f, -0.3f, 0.0f);
            }
        }
        if (Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D))
        {
            didMove = true;
            playerRenderer.sprite = playerFront;
            m_transform.rotation = Quaternion.Euler(0, 0, 0);
            m_transform.position -= new Vector3(0, 0.1f * speedModifier, 0);

            if (heldItem != null)
            {
                heldItem.transform.localPosition = new Vector3(-0.17f, -0.3f, 0.0f);
            }
        }
        if (Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.D))
        {
            didMove = true;
            playerRenderer.sprite = playerLeft;
            m_transform.rotation = Quaternion.Euler(0, 0, 0);
            m_transform.position -= new Vector3(0.1f * speedModifier, 0, 0);

            if (heldItem != null)
            {
                heldItem.transform.localPosition = new Vector3(0.02f, -0.3f, 0.0f);
            }
        }
        if (Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.W))
        {
            didMove = true;
            playerRenderer.sprite = playerRight;
            m_transform.rotation = Quaternion.Euler(0, 0, 0);
            m_transform.position += new Vector3(0.1f * speedModifier, 0f, 0);

            if (heldItem != null)
            {
                heldItem.transform.localPosition = new Vector3(0.02f, -0.3f, 0.0f);
            }
        }

        if (didMove)
        {
            if (!stepSound.isPlaying)
            {
                stepSound.Play();
            }
        }
        else
        {
            stepSound.Stop();
        }
    }

    private void PickupItem(AnswerItem item)
    {
        pickUpItem.Play();
        heldItem = item;
        item.GetComponent<CircleCollider2D>().isTrigger = true;

        heldItem.originalParent = heldItem.transform.parent;
        heldItem.originPos = heldItem.transform.localPosition;

        heldItem.transform.parent = this.transform;
        heldItem.GetComponent<SpriteRenderer>().sortingLayerName = "Held Item";
    }   

    private void CastRayForward()
    {
        playerRay.origin = m_transform.position;
        playerRay.direction = m_transform.up;

        RaycastHit2D hit = Physics2D.Raycast(m_transform.position, m_transform.up, 1, 1 << LayerMask.NameToLayer("AnswerItems"));

        if(hit)
        {
            Entity target = hit.collider.GetComponent<Entity>();
            if (target == null)
            {

            }
            else if (target is AnswerItem)
            {
                print(target.name);
                PickupItem((AnswerItem)target);
            } 
        }                    
    }

    private void DropHeldItem()
    { 
        if(heldItem != null)
        {
            print("I want to drop this");
            heldItem.transform.parent = heldItem.originalParent;
            heldItem.GetComponent<SpriteRenderer>().sortingLayerName = "Pedestal Item";
            StartCoroutine(ReturnItem());            
        }
        else
        {
            print("I'm not holding anything");
        }
    }
    private IEnumerator ReturnItem()
    {
        float interpVal = 0.0f;
        float timeToInterpolate = 2.0f;
        float timeToFinishMove = 0.0f;
        Vector3 currPos = heldItem.transform.position;
        GameObject trail = Instantiate(dropItemRenderer);
        trail.transform.parent = heldItem.transform;
        trail.transform.position = heldItem.transform.position;
        //heldItem.transform.position = heldItem.originPos;

        while (interpVal < 1)
        {
            timeToFinishMove += Time.deltaTime;

            interpVal = timeToFinishMove / timeToInterpolate;

            heldItem.transform.position = Vector3.Lerp(currPos, heldItem.originPos, interpVal);

            yield return null;
        }

        heldItem.GetComponent<CircleCollider2D>().isTrigger = false;
        GameObject.Destroy(trail);
        heldItem = null;
    }
}
